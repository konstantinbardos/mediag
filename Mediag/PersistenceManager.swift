//
//  NetworkManager.swift
//  Mediag
//
//  Created by Artur Vainola on 10/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

class PersistenceManager {
    
    var patientId: String
    let apiUrl = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com"
    
    init(patientId: String) {
        self.patientId = patientId
    }
    
    // Utility functions
    private func createRequest(url: String, method: String) -> URLRequest {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
    
    private func getDateFromString(date: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZZ"
        return dateFormatter.date(from: date)
    }
    
    // Http requests
    func getPatientInfo(responseHandler: @escaping (_ status: Int, _ message: String?, _ data: [String: Any]?) -> Void) {
        let request = createRequest(url: apiUrl + "/patients/" + patientId, method: "GET")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                //print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    var json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    if ((json?["dateOfBirth"]) != nil) {
                        json?["dateOfBirth"] = self.getDateFromString(date: json?["dateOfBirth"] as! String)
                    }
                    responseHandler(httpStatus.statusCode, "", json)
                }
            }
            //print("Get Patient Info Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func addPatient() {
        var request = createRequest(url: apiUrl + "/patients", method: "POST")
        request.httpBody = try? JSONSerialization.data(withJSONObject: ["id": patientId])
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                //responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    //responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    //responseHandler(httpStatus.statusCode, "", json)
                }
            }
            //print("Add Patient Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func addPatientInfo(info: [String: Any], responseHandler: @escaping (_ status: Int, _ message: String?, _ data: [String: Any]?) -> Void) {
        var request = createRequest(url: apiUrl + "/patients/" + patientId, method: "PUT")
        request.httpBody = try? JSONSerialization.data(withJSONObject: info)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                //print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    var json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    json?["dateOfBirth"] = self.getDateFromString(date: json?["dateOfBirth"] as! String)
                    responseHandler(httpStatus.statusCode, "", json)
                }
            }
            //print("Add Patient Info Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func getPatientTests(responseHandler: @escaping (_ status: Int, _ message: String?, _ data: [[String: Any]]?) -> Void) {
        let request = createRequest(url: apiUrl + "/patients/" + patientId + "/tests", method: "GET")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                //print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    var tests = try? JSONSerialization.jsonObject(with: data, options: []) as! [[String: Any]]
                    tests = tests?.map() { test in
                        var newTest = test
                        newTest["time"] = self.getDateFromString(date: test["time"] as! String)
                        return newTest
                    }
                    responseHandler(httpStatus.statusCode, "", tests)
                }
            }
            //print("Get Patient Tests Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func getTestData(id: String, responseHandler: @escaping (_ status: Int, _ message: String?, _ drawing: Drawing?) -> Void) {
        let request = createRequest(url: apiUrl + "/tests/" + id, method: "GET")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    let test = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(test!)
                    responseHandler(httpStatus.statusCode, "", Drawing(drawing: test!))
                    //json?["date"] = self.getDateFromString(date: json?["dateOfBirth"] as! String)
                    
                }
            }
            //print("Get Test Data Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func getTestResults(id: String, responseHandler: @escaping (_ status: Int, _ message: String?, _ results: [String: Any]?) -> Void) {
        let request = createRequest(url: apiUrl + "/tests/" + id + "/ds", method: "GET")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    let results = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(results!)
                    responseHandler(httpStatus.statusCode, "", results)
                }
            }
            //print("Get Test Results Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func getClockTestResults(id: String, responseHandler: @escaping (_ status: Int, _ message: String?, _ results: [String: Any]?) -> Void) {
        let request = createRequest(url: "http://ec2-52-59-229-31.eu-central-1.compute.amazonaws.com/api/digit", method: "GET")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    let results = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(results!)
                    responseHandler(httpStatus.statusCode, "", results)
                }
            }
            //print("Get Test Results Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func sendClock(drawing: Drawing, callback: @escaping (_ status: Int, _ message: String, _ data: [String: Any]?) -> Void) {
        var request = createRequest(url: "http://ec2-52-59-229-31.eu-central-1.compute.amazonaws.com/api/digit", method: "POST")
        let serializedDrawing = serializeData(data: drawing)
        request.httpBody = serializedDrawing
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                callback(-1, (error?.localizedDescription)!, nil)
                self.writeToFile(data: serializedDrawing!, filename: drawing.localId)
                return
            }
            if let httpStatus = response as? HTTPURLResponse {
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    self.writeToFile(data: serializedDrawing!, filename: drawing.localId)
                    callback(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    callback(httpStatus.statusCode, "Data was successfully sent.", json)
                }
            }
            //print("Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func getErrorDetectionResults(id: String, responseHandler: @escaping (_ status: Int, _ message: String?, _ results: [String: Any]?) -> Void) {
        let request = createRequest(url: apiUrl + "/tests/" + id + "/error_detection", method: "GET")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                responseHandler(-1, (error?.localizedDescription)!, nil)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse {
                print(httpStatus.statusCode)
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    responseHandler(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    let results = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(results!)
                    responseHandler(httpStatus.statusCode, "", results)
                }
            }
            //print("Get Test Results Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func send(drawing: Drawing, save: Bool = true, callback: @escaping (_ status: Int, _ message: String, _ data: [String: Any]?) -> Void) {
        var request = createRequest(url: apiUrl + "/tests", method: "POST")
        let serializedDrawing = serializeData(data: drawing)
        request.httpBody = serializedDrawing
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Error = \(error?.localizedDescription)")
                callback(-1, (error?.localizedDescription)!, nil)
                if save {
                    self.writeToFile(data: serializedDrawing!, filename: drawing.localId)
                }
                return
            }
            if let httpStatus = response as? HTTPURLResponse {
                if httpStatus.statusCode != 200 {
                    // check for http errors
                    print("StatusCode should be 200, but is \(httpStatus.statusCode)")
                    print("Response = \(response)")
                    self.writeToFile(data: serializedDrawing!, filename: drawing.localId)
                    callback(httpStatus.statusCode, (response?.description)!, nil)
                }
                else {
                    let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    callback(httpStatus.statusCode, "Data was successfully sent.", json)
                }
            }
            //print("Response String = \(String(data: data, encoding: .utf8))")
        }
        task.resume()
    }
    
    func serializeData(data drawing: Drawing) -> Data? {
        let drawing = drawing.getSerializableData()
        //print("Is valid JSON: ", JSONSerialization.isValidJSONObject(drawing))
        
        var data: Data?
        do {
            data = try JSONSerialization.data(withJSONObject: drawing)
            //print("Data: ", String(data: data!, encoding: String.Encoding.utf8)!)
        } catch (let error) {
            print("Error: ", error)
        }
        
        return data
    }
    
    private func deserializeData(data: Data) -> Drawing {
        //print("Is valid JSON: ", JSONSerialization.isValidJSONObject(data))
        //print("Data: ", String(data: data, encoding: String.Encoding.utf8)!)
     
        var drawing: [String:AnyObject]?
        do {
            try drawing = JSONSerialization.jsonObject(with: data) as? [String:AnyObject]
        } catch (let error){
            print("Error", error)
        }
        
        return Drawing(drawing: drawing!)!
    }
    
    func writeToFile(data: Data, filename: String) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let patienDir = dir.appendingPathComponent(patientId, isDirectory: true)
                try FileManager.default.createDirectory(at: patienDir, withIntermediateDirectories: true, attributes: nil)
                let path = patienDir.appendingPathComponent("/" + filename + ".json")
                //print("New file Path", path)
                try data.write(to: path, options: Data.WritingOptions.atomic)
            }
            catch (let error){
                print("Error: ", error)
            }
        }
    }
    
    func readFromFile(url: URL) -> Drawing? {
        var drawing: Drawing?
        do {
            let data = try Data(contentsOf: url, options: Data.ReadingOptions.alwaysMapped)
            drawing = deserializeData(data: data)
        }
        catch (let error){
            print("Error: ", error)
        }
        
        return drawing
    }
    
    func getAllFiles() -> [URL] {
        var files = [URL]()
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let subdir = patientId
                let path = dir.appendingPathComponent(subdir)
                files = try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: [])
                print(files)
            } catch (let error) {
                print("Error", error)
            }
        }
        
        return files
    }
    
    func getAllDrawings() -> [(url: URL, drawing: Drawing)] {
        let files = getAllFiles()
        var drawings = [(url: URL, drawing: Drawing)]()
        for file in files {
            drawings.append((file, readFromFile(url: file)!))
        }
        
        return drawings
    }
    
    func deleteFile(url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch(let error) {
            print("Error", error)
        }
    }
	
	class private func deserializeDataStatic(data: Data) -> Drawing {
		var drawing: [String:AnyObject]?
		do {
			try drawing = JSONSerialization.jsonObject(with: data) as? [String:AnyObject]
		} catch (let error){
			print("Error", error)
		}
		
		return Drawing(drawing: drawing!)!
	}
	
	class func readFromPreloadedFile(path: String) -> Drawing? {
		var drawing: Drawing?
		do {
			let str = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
			let data = str.data(using: String.Encoding.utf8)
			drawing = deserializeDataStatic(data: data!)
		}
		catch (let error){
			print("Error: ", error)
		}
		return drawing
	}
	
	class func existsPreloadedFile(name: String) -> Bool {
		let currentBundle = Bundle.main
		if currentBundle.path(forResource: name, ofType: "json", inDirectory: "Preloaded") != nil {
			return true
		} else {
			return false
		}
	}
	
	class func getPreloadedFilePath(name: String) -> String? {
		let currentBundle = Bundle.main
		let currentPath = currentBundle.path(forResource: name, ofType: "json", inDirectory: "Preloaded")
//		print("\(currentPath)")
		return currentPath
	}
	
	class func getPreloadedDrawing(name: String) -> Drawing {
		return readFromPreloadedFile(path: getPreloadedFilePath(name: name)!)!
	}

}
