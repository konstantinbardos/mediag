//
//  ResultsViewController.swift
//  Mediag
//
//  Created by Artur Vainola on 23/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit
import Charts

class XAxisLabelFormatter: NSObject, IAxisValueFormatter {
    private var labels: [String]?
    
    public init(_labels: [String])
    {
        super.init()
        self.labels = _labels
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        
        if index > (labels?.count)! - 1 {
            return String(value)
        }
        
        return labels![index]
    }
}

class ResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var testId: String!
    var persistenceManager: PersistenceManager?
    var results: [String: Any]? {
        didSet {
        }
    }
    /*
    var results = [[String: String]]() {
        didSet {
            tableView.reloadData()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    */
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var radarChartView: RadarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.isToolbarHidden = true
        
        /*
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
        let avgs = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0]
        let values = [22.0, 9.0, 14.0, 2.0, 16.0, 7.0]
        
        setChart(dataPoints: months, avgs: avgs, values: values)
        */
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let norm_avgs = [1.0,1.0,1.0,1.0,1.0]
        var norm_stdevs = [Double]()
        var norm_values = [Double]()
        
        for (index, _) in (results?["labels"] as! [String]).enumerated() {
            let avg = (results?["averages"] as! [Double])[index]
            let stdev = (results?["stdevs"] as! [Double])[index]
            //let avg_min = avg - stdev
            //let avg_max = avg + stdev
            let value = (results?["values"] as! [Double])[index]
            norm_stdevs.append(stdev / avg)
            norm_values.append(value / avg)
        }
        
        setChart(labels: results?["labels"] as! [String], avgs: norm_avgs, stdevs: norm_stdevs, values: norm_values)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (results?["labels"]! as! [String]).count 
    }
    
    /*
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Test results"
    }
    */
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Result", for: indexPath)
        if let resultCell = cell as? ResultsTableViewCell {
            
            let labels = results?["labels"] as! [String]
            let label = labels[indexPath.row]
            let averages = results?["averages"] as! [Double]
            let avg = averages[indexPath.row]
            let stdevs = results?["stdevs"] as! [Double]
            let stdev = stdevs[indexPath.row]
            let values = results?["values"] as! [Double]
            let value = values[indexPath.row]
            resultCell.result = ["label": label, "avg": avg, "stdev": stdev, "value": value]
            
        }
        return cell
    }
    
    func setChart(labels: [String], avgs: [Double], stdevs: [Double], values: [Double]) {
        
        var avgLowDataEntries: [ChartDataEntry] = []
        var avgHighDataEntries: [ChartDataEntry] = []
        var subjectDataEntries: [ChartDataEntry] = []
        
        for i in 0..<labels.count {
            let dataEntryAvgLow = RadarChartDataEntry(value: avgs[i] - stdevs[i])
            avgLowDataEntries.append(dataEntryAvgLow)
            let dataEntryAvgHigh = RadarChartDataEntry(value: avgs[i] + stdevs[i])
            avgHighDataEntries.append(dataEntryAvgHigh)
            let dataEntrySubject = RadarChartDataEntry(value: values[i])
            subjectDataEntries.append(dataEntrySubject)
        }
        
        //AVG LOW
        let radarChartDataSetAvgLow = RadarChartDataSet(values: avgLowDataEntries, label: "Average Low")
        radarChartDataSetAvgLow.colors = [.green]
        radarChartDataSetAvgLow.lineWidth = 2
        radarChartDataSetAvgLow.drawValuesEnabled = false
        //radarChartDataSetAvgLow.fillColor = UIColor.green
        //radarChartDataSetAvgLow.drawFilledEnabled = true
        //AVG HIGH
        let radarChartDataSetAvgHigh = RadarChartDataSet(values: avgHighDataEntries, label: "Average High")
        radarChartDataSetAvgHigh.colors = [.green]
        radarChartDataSetAvgHigh.lineWidth = 2
        radarChartDataSetAvgHigh.drawValuesEnabled = false
        //radarChartDataSetAvgHigh.fillColor = UIColor.green
        //radarChartDataSetAvgHigh.drawFilledEnabled = true
        //SUBJECT
        let radarChartDataSetTest = RadarChartDataSet(values: subjectDataEntries, label: "Subject")
        radarChartDataSetTest.lineWidth = 2
        radarChartDataSetTest.colors = [.red]
        radarChartDataSetTest.drawValuesEnabled = false
        //radarChartDataSetTest.fillColor = UIColor.red
        //radarChartDataSetTest.drawFilledEnabled = true
        let radarChartData = RadarChartData(dataSets: [radarChartDataSetAvgLow, radarChartDataSetAvgHigh, radarChartDataSetTest])
        radarChartData.labels = labels
        radarChartView.data = radarChartData
        radarChartView.chartDescription?.text = "Here we can have discription of the chart."

        
        radarChartView.yAxis.drawLabelsEnabled = false
        radarChartView.xAxis.drawLabelsEnabled = true
        radarChartView.xAxis.labelFont = UIFont(name: "Menlo", size: 12)!
        radarChartView.xAxis.valueFormatter = XAxisLabelFormatter(_labels: labels)
        radarChartView.animate(yAxisDuration: 1)
        
    }
    
    
    /*
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //self.results = persistenceManager.getResults(for: self.testId)
        self.results = [["label": "test1", "value": "123"], ["label": "test2", "value": "456"]]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.flashScrollIndicators()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Descriptive statistics"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Result", for: indexPath)
        let result = results[indexPath.row]
        cell.textLabel?.text = result["label"]!
        cell.detailTextLabel?.text = result["value"]!
        
        return cell
    }
    */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
