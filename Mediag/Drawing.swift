//
//  Drawing.swift
//  Mediag
//
//  Created by Artur Vainola on 08/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import Foundation

class Drawing: NSObject /*, NSCoding*/ {
    let localId = UUID().uuidString
    var id: String?
    var patientId: String
    var type: String
    var hand: String?
    var time = Date()
    var data = [[Point]]()
    
    init(_patientId: String, _type: String, _hand: String) {
        patientId = _patientId
        type = _type
        hand = _hand
    }
    
    init(id: String, type: String, hand: String, patientId: String, time: Date, data: [[Point]]) {
        self.id = id
        self.type = type
        self.hand = hand
        self.patientId = patientId
        self.time = time
        self.data = data
    }
 
    init?(drawing: [String: Any]) {
        self.id = drawing["id"] as? String
        self.type = drawing["type"] as! String
        self.hand = drawing["hand"] as? String
        self.patientId = drawing["patientId"] as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZZ"
        self.time = dateFormatter.date(from: drawing["time"] as! String)!
        var all = [[Point]]()
        if let lines = drawing["data"] as? [[[String: Double]]] {
            for line in lines {
                var pline = [Point]()
                for point in line {
                    pline.append(Point(deserializedData: point)!)
                }
                all.append(pline)
            }
        }
        self.data = all
    }
    
    func getSerializableData() -> [String: Any] {
        let points = self.data.map { (line:[Point]) -> [[String: Double]] in
            line.map({ (point: Point) -> [String: Double] in
                return point.getSerializableData()
            })
        }
        return [//"id": self.id,
                "type": self.type,
                "hand": self.hand as Any,
                "time": self.time.description,
                "patientId": self.patientId,
                "data": points] as [String : Any]
    }
}
