//
//  InfoViewController.swift
//  Mediag
//
//  Created by Artur Vainola on 10/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var patientId: String!
    var dateOfBirth: Date?
    var persistenceManager: PersistenceManager?
    @IBOutlet weak var testsView: UITableView!
    @IBOutlet weak var patientInfoView: UIStackView!
    
    @IBOutlet weak var patientID: UILabel!
    @IBOutlet weak var patientAge: UILabel!
    @IBOutlet weak var patientGender: UILabel!
    @IBOutlet weak var patientHandedness: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var addInfoButton: UIButton!
    
    var unsyncedTests = [(url: URL, drawing: Drawing)]() {
        didSet {
            if !unsyncedTests.isEmpty {
                syncTest(test: unsyncedTests[0])
            }
            testsView.reloadData()
        }
    }
    
    var syncedTests = [[String: Any]]() {
        didSet {
            testsView.reloadData()
        }
    }
    
    private func syncTest(test: (url: URL, drawing: Drawing)) {
        self.activityIndicator.startAnimating()
        persistenceManager?.send(drawing: test.drawing, save: false) { status, message, data in
            DispatchQueue.main.sync {
                if status == 200 {
                    self.persistenceManager?.deleteFile(url: test.url)
                    self.unsyncedTests.removeFirst()
                    self.fetchPatientTests()
                    self.testsView.reloadData()
                }
                self.activityIndicator.stopAnimating()
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        testsView.delegate = self
        testsView.dataSource = self
        self.patientID.text = patientId
    }
    
    func fetchPatientInfo() {
        persistenceManager?.getPatientInfo() { status, message, data in
            DispatchQueue.main.sync {
                if status == 200 {
                    if let dateOfBirth = data!["dateOfBirth"] as? Date {
                        self.dateOfBirth = dateOfBirth
                        let age = NSCalendar.current.dateComponents([.year] , from: dateOfBirth, to: Date()).year!
                        self.patientAge.text = String(describing: age)
                    }
                    if let sex = data?["sex"] as? String {
                        self.patientGender.text = sex.isEmpty ? "-" : sex
                    }
                    if let handedness = data?["hand"] as? String {
                        self.patientHandedness.text = handedness.isEmpty ? "-" : handedness
                    }
                    self.patientInfoView.setNeedsDisplay()
                }
                else if status == 404 {
                    self.persistenceManager?.addPatient()
                }
            }
        }
    }
    
    func fetchPatientTests() {
        persistenceManager?.getPatientTests() { status, message, data in
            DispatchQueue.main.sync {
                if status == 200 {
                    if data != nil {
                        self.syncedTests = data!
                    }
                }
                else {
                    // TODO: display error message to user
                }
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        if let selectedRow = self.testsView.indexPathForSelectedRow {
            self.testsView.deselectRow(at: selectedRow, animated: true)
        }
        self.navigationController?.isToolbarHidden = true
        unsyncedTests = persistenceManager!.getAllDrawings().sorted(by: { (drawing1: (url: URL, drawing: Drawing), drawing2: (url: URL, drawing: Drawing)) -> Bool in
            return drawing1.drawing.time > drawing2.drawing.time
        })
        fetchPatientInfo()
        fetchPatientTests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.testsView.flashScrollIndicators()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return unsyncedTests.count
        } else {
            return syncedTests.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Unsynced tests"
        }
        else {
            return "Synced tests"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "UnsyncedTest", for: indexPath)
            let test = unsyncedTests[indexPath.row]
            cell.textLabel?.text = TestsPreloaded.getLabel(forName: test.drawing.type) + test.drawing.hand!
            cell.detailTextLabel?.text = test.drawing.time.description(with: Locale.current) //test.drawing.id
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "SyncedTest", for: indexPath)
            let test = syncedTests[indexPath.row]
			
			let strType = test["type"] as! String?
			let strID = test["id"] as! String?
			var strName = TestsPreloaded.getLabel(forName: strType!) ?? "unknown"
            if let hand = test["hand"] as! String? {
                strName = strName + " " + hand + " hand"
            }
			let timeStr = (test["time"] as! Date?)?.description(with: .current)
            cell.textLabel?.text = strName + " (id: " + strID! + ")"
			cell.detailTextLabel?.text = timeStr
			
            if !((test["results"] as? [String])?.isEmpty)! {
                cell.accessoryType = UITableViewCellAccessoryType.detailDisclosureButton
            }
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "AddInfo":
                if let modalVC = segue.destination as? AddPatientInfoViewController {
                    modalVC.persistanceManager = self.persistenceManager
                    modalVC.patientId = self.patientId
                    modalVC.gender = self.patientGender.text
                    modalVC.handedness = self.patientHandedness.text
                    if self.dateOfBirth != nil {
                        modalVC.dateOfBirth = self.dateOfBirth!
                    }
                    modalVC.infoVC = self
                }
            case "Draw":
                if let drawvc = segue.destination as? TestViewController {
					drawvc.patientId = self.patientId
					drawvc.navigationItem.title = "Patient: " + self.patientId
					drawvc.persistenceManager = self.persistenceManager!
                }
                break
            case "ViewUnsynced":
                if let cell = sender as? UITableViewCell,
                    let indexPath = testsView.indexPath(for: cell),
                    let segueToVC = segue.destination as? DrawViewController {
                    segueToVC.drawing = unsyncedTests[indexPath.row].drawing
                    segueToVC.drawingType = unsyncedTests[indexPath.row].drawing.type
                    segueToVC.finished = true
                }
                break
            case "ViewSynced":
                if let cell = sender as? UITableViewCell,
                    let indexPath = testsView.indexPath(for: cell),
                    let segueToVC = segue.destination as? DrawViewController {
                    let test = syncedTests[indexPath.row]
					
					segueToVC.drawingType = test["type"] as! String?
                    segueToVC.finished = true
					
                    let stats = UIBarButtonItem(title: "Stats", style: .plain, target: segueToVC, action: #selector(segueToVC.statsTapped(_:)))
                    segueToVC.navigationItem.setRightBarButton(stats, animated: true)
                    segueToVC.persistenceManager = self.persistenceManager!
                    persistenceManager?.getTestData(id: test["id"] as! String) {
                        status, message, drawing in
                        DispatchQueue.main.sync {                            
                            if status == 200 {
                                segueToVC.drawing = drawing
                            }
                        }
                    }
                }
                break
            default:
                break
            }
        }
        
    }
    
}
