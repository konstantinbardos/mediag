//
//  DrawViewController.swift
//  Mediag
//
//  Created by Artur Vainola on 01/10/2016.
//  Copyright © 2016 Artur Vainola. All rights reserved.
//

import UIKit

class DrawViewController: UIViewController, DrawViewDelegate, UIPopoverPresentationControllerDelegate {
	
    var patientId: String!
    var finished = false // drawing is finished
    var drawing: Drawing? {
        didSet {
            if drawView != nil {
                drawView.setDrawing(_drawing: drawing!)
            }
            
            if self.drawing?.id != nil && self.drawingType == "sine_trace" {
                print(1321312312321)
                self.activityIndicator.startAnimating()
                self.persistenceManager?.getErrorDetectionResults(id: (self.drawing?.id)!) { status, message, data in
                    DispatchQueue.main.sync {
                        print(98219631723)
                        if status == 200 {
                            print(data!)
                            for error in (data?["data"] as? [[String: Any]])! {
                                let center = error["center"] as! [String: Any]
                                let centerX = center["x"] as! Double
                                let centerY = center["y"] as! Double
                                let radius = error["radius"] as! Double
                                self.drawView.drawCircle(centerX: centerX, centerY: centerY, radius: radius, color: .red)
                            }
                            
                            let stats = UIBarButtonItem(title: "Stats", style: .plain, target: self, action: #selector(self.statsTapped(_:)))
                            self.navigationItem.setRightBarButton(stats, animated: true)
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
            }
        }
    }
	var drawingType : String!
    var drawingHand: String!
	
    var persistenceManager: PersistenceManager?
	
    var doneButton: UIBarButtonItem?
    var clearButton: UIBarButtonItem?
    
    var testResults: [String: Any]?

    @IBOutlet weak var drawView: DrawView!
	@IBOutlet weak var imageTemp: UIImageView!
	@IBOutlet weak var testModeButton: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
    override func viewDidLoad() {
        self.doneButton?.isEnabled = false
        self.clearButton?.isEnabled = false
        self.activityIndicator.hidesWhenStopped = true
		
		self.navigationController?.isToolbarHidden = false
		self.drawView.type = drawingType
        self.drawView.hand = drawingHand
		
    }


    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = false
        //print(drawing)
        if drawing != nil {
            drawView.setDrawing(_drawing: drawing!)
        }
    }
	
    override func viewDidLayoutSubviews() {
        drawView.patientId = self.patientId
        drawView.delegate = self
        if finished {
            drawView.disableEdit()
        }
    }
    
    func drawingStarted(drawView: DrawView) {
        self.doneButton?.isEnabled = true
        self.clearButton?.isEnabled = true
    }
    
	@IBAction func clearTapped(_ sender: UIBarButtonItem) {
		drawView.clear()
        self.doneButton?.isEnabled = false
        self.clearButton?.isEnabled = false
		drawView.setNeedsDisplay()
	}
	
    @IBAction func scalingChanged(_ sender: UISlider) {
        drawView.widthScaling = CGFloat(sender.value)
        drawView.setNeedsDisplay()
    }
	
	@IBAction func testModeTapped(_ sender: AnyObject) {
		drawView.isAdvancedMode = !(drawView.isAdvancedMode)
		drawView.setNeedsDisplay()
	}
	
	@IBAction func drawingModeButtonTapped(_ sender: UIBarButtonItem) {
		if let mode = sender.title {
			switch mode {
			case "Dots" :
				drawView.setDrawingMode(mode: .Dots)
			case "Lines" :		drawView.setDrawingMode(mode: .Lines)
			case "Pressure" :	drawView.setDrawingMode(mode: .Pressure)
            default :			drawView.setDrawingMode(mode: .Lines)
			}
			drawView.setNeedsDisplay()
		}
	}
	
    @IBAction func doneTapped(_ sender: UIBarButtonItem) {
        self.activityIndicator.startAnimating()
        self.finished = true
        self.doneButton?.isEnabled = false
        self.clearButton?.isEnabled = false
        
        if self.drawingType == "clock" {
            persistenceManager?.sendClock(drawing: drawView.getDrawing()!) { status, message, data in
                DispatchQueue.main.sync {
                    if status == 200 {
                        //print(data)
                        let circle = data?["circle"] as! [String: Any]
                        let center = circle["center"] as! [String: Any]
                        let centerX = center["x"] as! Double
                        let centerY = center["y"] as! Double
                        let radius = circle["radius"] as! Double
                        self.drawView.drawCircle(centerX: centerX, centerY: centerY, radius: radius, color: .green)
                        let digits = data?["digits"]
                        self.drawView.drawDigits(digits: digits as! [[String: Any]])
                        
                        //self.performSegue(withIdentifier: "Results", sender: data)
                    } else {
                        let info = "Unable to get test results."
                        self.showAlert(title: "Error", message: message + "\n" + info)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }
        }
        else if self.drawingType == "sine_trace" {
            persistenceManager?.send(drawing: drawView.getDrawing()!) { status, message, data in
                    if status == 200 {
                        print(data!)
                        self.testResults = data
                        //self.performSegue(withIdentifier: "Stats", sender: data)
                        
                        self.persistenceManager?.getErrorDetectionResults(id: data?["id"] as! String) { status, message, data in
                            DispatchQueue.main.sync {
                                if status == 200 {
                                    print(data!)
                                    for error in (data?["data"] as? [[String: Any]])! {
                                        let center = error["center"] as! [String: Any]
                                        let centerX = center["x"] as! Double
                                        let centerY = center["y"] as! Double
                                        let radius = error["radius"] as! Double
                                        self.drawView.drawCircle(centerX: centerX, centerY: centerY, radius: radius, color: .red)
                                    }
                                    
                                    let stats = UIBarButtonItem(title: "Stats", style: .plain, target: self, action: #selector(self.statsTapped(_:)))
                                    self.navigationItem.setRightBarButton(stats, animated: true)
                                    self.activityIndicator.stopAnimating()
                                }
                            }
                        }
                    } else {
                        let info = "Data saved locally. Will try to sync again later."
                        self.showAlert(title: "Error", message: message + "\n" + info)
                    }
            }
        }
        else {
            persistenceManager?.send(drawing: drawView.getDrawing()!) { status, message, data in
                DispatchQueue.main.sync {
                    if status == 200 {
                        //print(data)
                        self.performSegue(withIdentifier: "Stats", sender: data)
                    } else {
                        let info = "Data saved locally. Will try to sync again later."
                        self.showAlert(title: "Error", message: message + "\n" + info)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    @IBAction func statsTapped(_ sender: UIBarButtonItem) {
        self.activityIndicator.startAnimating()
        
        print(self.drawingType)
        
        if self.drawingType == "clock" {
            
            persistenceManager?.getClockTestResults(id: (drawing?.id)!) { status, message, data in
                DispatchQueue.main.sync {
                    if status == 200 {
                        print(data as Any!)
                        //self.performSegue(withIdentifier: "Results", sender: data)
                    } else {
                        let info = "Unable to get test results."
                        self.showAlert(title: "Error", message: message! + "\n" + info)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }
        }
        else if (self.testResults != nil) {
            self.performSegue(withIdentifier: "Stats", sender: self.testResults)
        }
        else {
            persistenceManager?.getTestResults(id: (drawing?.id)!) { status, message, data in
                DispatchQueue.main.sync {
                    if status == 200 {
                        //print(data)
                        self.performSegue(withIdentifier: "Stats", sender: data)
                    } else {
                        let info = "Unable to get test results."
                        self.showAlert(title: "Error", message: message! + "\n" + info)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
            })
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
	
	@IBAction func back(_ sender: UIBarButtonItem) {
        if self.finished || (drawView.getDrawing() == nil) {
            // Go back to the previous ViewController
            goBack()
        }
        else {
            let alertController = UIAlertController(
                title: "Are your sure to go back ?",
                message: "Your data will be erased",
                preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: { action in
                // Go back to the previous ViewController
                self.goBack()
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
	}
    
    /*
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "Results", self.drawingType == "clock" {
            
            //TODO: Send data and draw boxes and numbers
            
            return false
        }
        return true
    }
    */
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showSettings" {
			if let popVC = segue.destination as? DrawViewSettingsController {
				if let poppc = popVC.popoverPresentationController {
					poppc.delegate = self
				}
				popVC.mainView = drawView
				popVC.valueAdvanced = drawView.isAdvancedMode
				popVC.valueFinger = drawView.isFingerInputRestricted
				popVC.valueDrawingMode = drawView.drawingModeIndex
				popVC.valueLineWidth = Float(drawView.widthScaling)
			}
		}
        else if segue.identifier == "Stats" {
            if let resultsVC = segue.destination as? ResultsViewController {
                resultsVC.results = sender as! [String : Any]?
                resultsVC.persistenceManager = self.persistenceManager
            }
        }
    }
}

